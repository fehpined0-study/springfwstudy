package com.springtutorial.util.logger.interfaces;

import java.util.logging.Level;

/**
 * Interface for formatting and logging a message using the default implementations
 * for the java.util.logging library.
 * <br><br>
 * Although Java and juli already have a formatting tool for logs (using {@code LogRecord}s and formatters)
 * it does not enable for deep logging logic customization. As such, this interface aims to offer great
 * customization capabilities by enabling for log formatting in the {@code Log} object, instead of the formatter.
 * <br><br>
 * Implementations of this inteface should format the log using the {@code format} method in any suitable way. The message
 * can then be logged using any logger of choosing.
 */
public interface FormatLogger {
  public String format(Level logLevel, String callerClass, String callerMethod, Object logContent);
}
