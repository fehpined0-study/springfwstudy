package com.springtutorial.util.logger;

import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.juli.logging.Log;

import com.springtutorial.util.logger.formatloggers.DevelopmentLoggerFormatter;
import com.springtutorial.util.logger.formatloggers.KubernetesLoggerFormatter;
import com.springtutorial.util.logger.interfaces.FormatLogger;

/**
 * Custom Logger for usage within the Spring Framework. Implementation is made in such
 * a way so that it can be used in conjunction with Apache Tomcat server and Catalina, so that
 * messages logged from these are also formatted accordingly.
 * <br><br>
 * To acomplish this, the custom logger aims to format the message in a different way from the
 * {@code org.apache.juli.logging} and {@code java.util.logging}, by formatting the message before
 * serializing it to a {@code LogRecord}, passing it down to the {@code java.util.logging::Logger}
 * as a already formatted message.
 * <br><br>
 * For this, when this class is instantiated and used, the default formatter for the {@code ConsoleHandler}
 * logger is overriden to a bare-bones implementation that only returns the message, because it is considered
 * already formatted.
 * <br><br>
 * TODO add support for custom formatters
 */
public class CustomSpringLogger implements Log {
  public static final String LOGGER_FORMATTER_OPTION_PROP = "com.springtutorial.kubernetesEnvironment";
  
  private Logger logger;
  private FormatLogger formatter;

  public CustomSpringLogger() {
    String formatterOpt = System.getProperty(LOGGER_FORMATTER_OPTION_PROP);
    
    if (formatterOpt != null && formatterOpt.equals("true")) {
      this.formatter = new KubernetesLoggerFormatter();
    } else {
      this.formatter = new DevelopmentLoggerFormatter();
    }

    // Setting a bare-bones formatter - all the formatting is happening here anyways
    Logger rootLogger = Logger.getLogger("");

    // Implementation stolen from DirectJDKLog, SpringFramework
    for (Handler handler : rootLogger.getHandlers()) {
      if (handler instanceof ConsoleHandler) {
        handler.setFormatter(new SimpleFormatter());
      }
    }
  }

  public CustomSpringLogger(String name) {
    String formatterOpt = System.getProperty(LOGGER_FORMATTER_OPTION_PROP);
    
    if (formatterOpt != null && formatterOpt.equals("true")) {
      this.formatter = new KubernetesLoggerFormatter();
    } else {
      this.formatter = new DevelopmentLoggerFormatter();
    }

    this.logger = Logger.getLogger(name);
  }

  @Override
  public boolean isDebugEnabled() {
    return true;
  }

  @Override
  public boolean isErrorEnabled() {
    return true;
  }

  @Override
  public boolean isFatalEnabled() {
    return true;
  }

  @Override
  public boolean isInfoEnabled() {
    return true;
  }

  @Override
  public boolean isTraceEnabled() {
    return true;
  }

  @Override
  public boolean isWarnEnabled() {
    return true;
  }

  @Override
  public void trace(Object message) {
    StackTraceElement stk[] = Thread.currentThread().getStackTrace();
    String formattedMessage = formatter.format(Level.FINEST, stk[2].getClassName(), stk[2].getMethodName(), message);
    logger.log(Level.FINEST, formattedMessage);
  }

  @Override
  public void trace(Object message, Throwable t) {
    StackTraceElement stk[] = Thread.currentThread().getStackTrace();
    String formattedMessage = formatter.format(Level.FINEST, stk[2].getClassName(), stk[2].getMethodName(), message);
    logger.log(Level.FINEST, formattedMessage);
  }

  @Override
  public void debug(Object message) {
    StackTraceElement stk[] = Thread.currentThread().getStackTrace();
    String formattedMessage = formatter.format(Level.FINER, stk[2].getClassName(), stk[2].getMethodName(), message);
    logger.log(Level.FINER, formattedMessage);
  }

  @Override
  public void debug(Object message, Throwable t) {
    StackTraceElement stk[] = Thread.currentThread().getStackTrace();
    String formattedMessage = formatter.format(Level.FINER, stk[2].getClassName(), stk[2].getMethodName(), message);
    logger.log(Level.FINER, formattedMessage);
  }

  @Override
  public void info(Object message) {
    StackTraceElement stk[] = Thread.currentThread().getStackTrace();
    String formattedMessage = formatter.format(Level.INFO, stk[2].getClassName(), stk[2].getMethodName(), message);
    logger.log(Level.INFO, formattedMessage);
  }

  @Override
  public void info(Object message, Throwable t) {
    StackTraceElement stk[] = Thread.currentThread().getStackTrace();
    String formattedMessage = formatter.format(Level.INFO, stk[2].getClassName(), stk[2].getMethodName(), message);
    logger.log(Level.INFO, formattedMessage);
  }

  @Override
  public void warn(Object message) {
    StackTraceElement stk[] = Thread.currentThread().getStackTrace();
    String formattedMessage = formatter.format(Level.WARNING, stk[2].getClassName(), stk[2].getMethodName(), message);
    logger.log(Level.WARNING, formattedMessage);
  }

  @Override
  public void warn(Object message, Throwable t) {
    StackTraceElement stk[] = Thread.currentThread().getStackTrace();
    String formattedMessage = formatter.format(Level.WARNING, stk[2].getClassName(), stk[2].getMethodName(), message);
    logger.log(Level.WARNING, formattedMessage);
  }

  @Override
  public void error(Object message) {
    StackTraceElement stk[] = Thread.currentThread().getStackTrace();
    String formattedMessage = formatter.format(Level.SEVERE, stk[2].getClassName(), stk[2].getMethodName(), message);
    logger.log(Level.SEVERE, formattedMessage);
  }

  @Override
  public void error(Object message, Throwable t) {
    StackTraceElement stk[] = Thread.currentThread().getStackTrace();
    String formattedMessage = formatter.format(Level.SEVERE, stk[2].getClassName(), stk[2].getMethodName(), message);
    logger.log(Level.SEVERE, formattedMessage);
  }

  @Override
  public void fatal(Object message) {
    StackTraceElement stk[] = Thread.currentThread().getStackTrace();
    String formattedMessage = formatter.format(Level.SEVERE, stk[2].getClassName(), stk[2].getMethodName(), message);
    logger.log(Level.SEVERE, formattedMessage);
  }

  @Override
  public void fatal(Object message, Throwable t) {
    StackTraceElement stk[] = Thread.currentThread().getStackTrace();
    String formattedMessage = formatter.format(Level.SEVERE, stk[2].getClassName(), stk[2].getMethodName(), message);
    logger.log(Level.SEVERE, formattedMessage);
  }
}