package com.springtutorial.util.logger;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BusinessData<T extends Object> {
  @JsonProperty
  private T data;

  public BusinessData(T data) {
    this.data = data;
  }
}
