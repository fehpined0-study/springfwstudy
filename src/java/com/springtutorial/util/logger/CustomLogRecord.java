package com.springtutorial.util.logger;

public class CustomLogRecord {
  public String message;
  public BusinessData<?> businessData;

  public CustomLogRecord setMessage(String message) {
    this.message = message;
    return this;
  }

  public CustomLogRecord setBusinessData(BusinessData<?> data) {
    this.businessData = data;
    return this;
  }
}
