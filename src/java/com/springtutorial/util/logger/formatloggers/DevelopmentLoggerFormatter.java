package com.springtutorial.util.logger.formatloggers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.springtutorial.util.logger.CustomLogRecord;
import com.springtutorial.util.logger.interfaces.FormatLogger;

public class DevelopmentLoggerFormatter implements FormatLogger {
  private ObjectMapper JSON;

  public DevelopmentLoggerFormatter() {
    this.JSON = new ObjectMapper();
  }
  
  @Override
  public String format(Level logLevel, String callerClass, String callerMethod, Object logContent) {
    // [{Level} | {DateTime} | {Class} -> {Method}] - {Message} | Business Data: {BusinessData || none}
    String[] fullClassName = callerClass.split("\\.");
    String shortClassName = fullClassName[fullClassName.length - 1];
    String message = "";
    String businessData = "none";

    if (logContent instanceof CustomLogRecord) {
      message = ((CustomLogRecord) logContent).message;

      try {
        businessData = JSON.writeValueAsString(((CustomLogRecord) logContent).businessData);
      } catch (Exception ignored) {
        // fuck
        businessData = "Tried to serialize, but throwed exception!";
      }
    } else {
      // Best effort logging
      message = String.valueOf(logContent);
    }

    String formattedMessage = "[" + logLevel.toString() + " | " +
      new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()) + " | " +
      shortClassName + " -> " +
      callerMethod + "] - " +
      message + " | " +
      "Business Data: " + businessData;

    if (logLevel.toString() == "SEVERE") {
      formattedMessage = "\u001B[31m" + formattedMessage + "\u001B[0m";
    }

    if (logLevel.toString() == "WARNING") {
      formattedMessage = "\u001B[33m" + formattedMessage + "\u001B[0m";
    }

    return formattedMessage;
  }
}
