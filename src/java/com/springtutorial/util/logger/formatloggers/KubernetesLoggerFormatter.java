package com.springtutorial.util.logger.formatloggers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.springtutorial.util.logger.CustomLogRecord;
import com.springtutorial.util.logger.interfaces.FormatLogger;

public class KubernetesLoggerFormatter implements FormatLogger {
  protected ObjectMapper JSON;

  public KubernetesLoggerFormatter() {
    this.JSON = new ObjectMapper();
  }

  @Override
  public String format(Level logLevel, String callerClass, String callerMethod, Object logContent) {
    // {level: $level$, date: $date$, sourceClass: $class$, sourceMethod: $method$, businessData: $businessDataObj$}
    Object objMessage;

    if (logContent.getClass().isInstance(CustomLogRecord.class)) {
      CustomLogRecord record = ((CustomLogRecord) logContent);

      objMessage = new Object() {
        @JsonProperty
        String level = logLevel.toString();

        @JsonProperty
        String date = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date());

        @JsonProperty
        String sourceClass = callerClass;

        @JsonProperty
        String sourceMethod = callerMethod;

        @JsonProperty
        String message = record.message;

        @JsonProperty
        Object businessData = record.businessData;
      };
    } else {
      // Best effort logging
      objMessage = new Object() {
        @JsonProperty
        String level = logLevel.toString();

        @JsonProperty
        String date = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date());

        @JsonProperty
        String sourceClass = callerClass;

        @JsonProperty
        String sourceMethod = callerMethod;

        @JsonProperty
        String message = String.valueOf(logContent);

        @JsonProperty
        Object businessData = null;
      };
    }

    String messageToLog = "";

    try {
      messageToLog = JSON.writeValueAsString(objMessage);
    } catch (JsonProcessingException ignored) {
      messageToLog = "Failed to serialize businessData log object!";
    }

    return String.valueOf(messageToLog);
  }
}
