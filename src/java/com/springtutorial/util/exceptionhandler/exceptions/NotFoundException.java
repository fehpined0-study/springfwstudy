package com.springtutorial.util.exceptionhandler.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends Exception {
  public String slug;
  public String message;
  public int code;
  
  public NotFoundException(String message) {
    this.message = message;
    this.code = HttpStatus.NOT_FOUND.value();
    this.slug = "NOT_FOUND";
  }

  public NotFoundException() {
    this.message = "Not Found";
    this.code = HttpStatus.NOT_FOUND.value();
    this.slug = "NOT_FOUND";
  }
}

