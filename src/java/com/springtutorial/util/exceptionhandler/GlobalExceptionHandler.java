package com.springtutorial.util.exceptionhandler;

import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.springtutorial.util.exceptionhandler.exceptions.BadRequestException;
import com.springtutorial.util.exceptionhandler.exceptions.NotFoundException;
import com.springtutorial.util.exceptionhandler.interfaces.FieldErrorData;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@ControllerAdvice
public class GlobalExceptionHandler {

  @ExceptionHandler(BadRequestException.class)
  @ResponseBody
  Object handleBadRequest(HttpServletRequest req, BadRequestException exception) {
      return new Object() {
        @JsonProperty
        String slug = exception.slug;

        @JsonProperty
        String message = exception.message;

        @JsonProperty
        int code = exception.code;

        @JsonProperty
        FieldErrorData[] errors = exception.fieldErrors;
      };
  } 

  @ExceptionHandler(NotFoundException.class)
  @ResponseBody
  Object handleNotFound(HttpServletRequest req, NotFoundException exception) {
      return new Object() {
        @JsonProperty
        String slug = exception.slug;

        @JsonProperty
        String message = exception.message;

        @JsonProperty
        int code = exception.code;
      };
  } 

  @ExceptionHandler(Exception.class)
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  @ResponseBody
  public Object handleAny(HttpServletResponse res, Exception exception) throws Exception {
    System.out.println(exception.getClass().getName());
    if (AnnotationUtils.findAnnotation(exception.getClass(), ResponseStatus.class) != null) {
      throw exception;
    }

    return new Object() {
      @JsonProperty
      String slug = "INTERNAL_SERVER_ERROR";

      @JsonProperty
      String message = "Internal Server Error";

      @JsonProperty
      int code = HttpStatus.INTERNAL_SERVER_ERROR.value();
    };
  } 
}
