package com.springtutorial.util.exceptionhandler.interfaces;

public class FieldErrorData {
  public String field;
  public String validationFail;

  public FieldErrorData(String field, String validationError) {
    this.field = field;
    this.validationFail = validationError;
  }
}
