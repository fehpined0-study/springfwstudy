package com.springtutorial.app.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springtutorial.app.controllers.dto.GetHelloControllerInputDTO;
import com.springtutorial.app.services.HelloWorldService;
import com.springtutorial.app.services.dto.GetHelloServiceOutputDTO;
import com.springtutorial.util.exceptionhandler.exceptions.NotFoundException;

import jakarta.inject.Inject;
import jakarta.inject.Named;

@RestController("HELLOWORLD_CONTROLLER")
@RequestMapping("/hello")
public class HelloWorldController {

  // ----------------------------- Injections
  @Inject()
  @Named("HELLOWORLD_SERVICE")
  private HelloWorldService service;

  // ----------------------------- Rest Mappings

  @GetMapping("/gethello")
  public GetHelloServiceOutputDTO sayHello(@RequestBody GetHelloControllerInputDTO input) throws NotFoundException {
    return service.getHello(input.id);
  }

  @GetMapping("env")
  public String getDefaultGreet() {
    return this.service.getEnvGreet();
  }
}
