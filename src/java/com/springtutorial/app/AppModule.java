package com.springtutorial.app;

import org.springframework.stereotype.Component;

import com.springtutorial.app.controllers.HelloWorldController;

import jakarta.inject.Named;

@Component
public class AppModule {
  @Named("HELLOWORLD_CONTROLLER")
  private HelloWorldController controller;

  public void start() {
    
  }
}