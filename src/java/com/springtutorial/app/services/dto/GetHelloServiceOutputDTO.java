package com.springtutorial.app.services.dto;

public class GetHelloServiceOutputDTO {
  public String greeting;
  public String name;

  public GetHelloServiceOutputDTO(String greet, String name) {
    this.greeting = greet;
    this.name = name;
  }
}
