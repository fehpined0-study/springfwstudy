package com.springtutorial.app.services;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.springtutorial.app.services.dto.GetHelloServiceOutputDTO;
import com.springtutorial.config.ConfigProperties;
import com.springtutorial.util.exceptionhandler.exceptions.NotFoundException;

import jakarta.inject.Inject;

@Service("HELLOWORLD_SERVICE")
public class HelloWorldService {
  @Inject
  private ConfigProperties config;

  public GetHelloServiceOutputDTO getHello(String greetId) throws NotFoundException {
    Map<String, GetHelloServiceOutputDTO> greetMap = new HashMap<>();
    greetMap.put("F-01", new GetHelloServiceOutputDTO("Hello Sir", "Fernando"));
    greetMap.put("P-02", new GetHelloServiceOutputDTO("Hello Mr.", "Paulo"));

    if (!greetMap.containsKey(greetId)) {
      throw new NotFoundException("Greet not found!");
    }

    return greetMap.get(greetId);
  }

  public String getEnvGreet() {
    return config.defaultGreet;
  }
}
