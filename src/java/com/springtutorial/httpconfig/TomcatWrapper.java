package com.springtutorial.httpconfig;

import java.io.FileReader;
import java.net.URL;
import java.util.Properties;

import org.apache.catalina.LifecycleException;
import org.apache.catalina.LifecycleState;
import org.apache.catalina.startup.Tomcat;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;

public class TomcatWrapper {
  private final String TOMCAT_SERVER_PORT_PROPERTY = "tomcat.server.port";

  private Tomcat httpServer;
  private Log logger;
  private String baseDirPath;
  private Properties props;
  private int port;

  public TomcatWrapper() {
    // Setting properties from active profile
    this.props = new Properties();
    try {
      URL activeProps = this.getClass().getClassLoader().getResource("application-" + System.getProperty("spring.profiles.active") + ".properties");
      props.load((new FileReader(activeProps.getFile())));
    } catch (Exception ignored) {
      props.setProperty("server.port", "8080");
    }

    // Setting server parameters
    this.baseDirPath = "./src/resources/tomcat";
    this.port = isPropertyValid(TOMCAT_SERVER_PORT_PROPERTY) ?
      Integer.parseInt(props.getProperty(TOMCAT_SERVER_PORT_PROPERTY)) :
      8080;

    // Setting aspects
    this.logger = LogFactory.getLog(getClass());
  }

  public Tomcat getDefaultTomcatImpl() {
    if (httpServer == null) {
      httpServer = new Tomcat();
      httpServer.setBaseDir(baseDirPath);
      httpServer.getHost().setAppBase(".");
      httpServer.addWebapp("", ".");
      httpServer.setPort(port);
      httpServer.getConnector();
    }

    return httpServer;
  }

  public void initialize() throws LifecycleException {
    Tomcat server = getDefaultTomcatImpl();
    
    logger.info("Starting Tomcat server on port: " + this.port);

    if (server.getServer().getState() != LifecycleState.STARTED) {
      httpServer.start();
      httpServer.getServer().await();
    }
  };

  private boolean isPropertyValid(String propertyKey) {
    return this.props.containsKey(propertyKey) && this.props.getProperty(propertyKey).length() != 0;
  }
}
