package com.springtutorial;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Configuration
@ComponentScan("com.springtutorial")
@PropertySource("classpath:application-${spring.profiles.active}.properties")
public class AppConfig extends WebMvcConfigurationSupport {
  
}
