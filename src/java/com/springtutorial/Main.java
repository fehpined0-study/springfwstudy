package com.springtutorial;

import org.apache.catalina.LifecycleException;

import com.springtutorial.httpconfig.TomcatWrapper;

public class Main {
  public static void main(String[] args) throws LifecycleException {
    new TomcatWrapper().initialize();
  }
}