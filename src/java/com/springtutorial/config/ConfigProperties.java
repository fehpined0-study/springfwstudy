package com.springtutorial.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ConfigProperties {
  @Value("${application.default.greet:Default Hello}")
  public String defaultGreet;
}
